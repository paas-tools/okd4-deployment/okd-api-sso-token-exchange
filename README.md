CERN SSO OAuth token exchange for OKD API token
===============================================

This is a simple web server written in `Go` whose goal is, in short, to exchange an CERN SSO oauth token (which has to be issued for specific `audience/Application`) for a token
which can be used to authenticate to specific OKD4 cluster API (OKD API token).

# How this works

Once a request is sent to `/openshift-api-token` the following things happen:

1. We first check if valid `Authorization` header is present in the request (`Authorization: Bearer <jwtToken>`)
2. We make sure that an additional query parameter - `redirect-uri` is present in the URL - this is later used as the `RedirectURI` of the `OAuthAccessToken`
3. Afterwards we make sure that the `jwtToken` is a valid and non-expired access token issued for specific audience (the audience is configured with `-audience` command argument, e.g. `./okd-api-sso-token-exchange -audience=test`)
4. If the validation is successful we proceed by checking if there is already a `User` matching the following criteria `user.Name == jwtToken['sub']`. If that is not the case we simply create it using the data stored in `jwtToken` claims
5. Finally, we create an `OAuthAccessToken` resource and return the access code to the client

With such token we can now make requests to the OKD4 API on behalf of a specific user.

# Original usecase which motivated the creation of this application

* Application A (e.g. WebSevices Portal) uses central Keycloak SSO for user authentication. The result of a successful login is an access token issued
specifically for this application.
* Application B (OKD4 cluster) uses the same central Keycloak SSO for user authentication however the token issued for Application A can't be
used to authenticate to Application B

It is worth mentioning that we could make use of the [Token Exchange](https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/) feature of the Keycloak server, however due to the way OKD `oauth-server` works it is not
possible (it would be pretty hard to feed the exchanged token to `openshift-oauth` without any user interaction). To be more precise, in order to get a valid Openshift API token we would need to follow the full authorization code
flow which requires user interaction and this is not suitable for non-browser clients.

# Requirements

In order to exchange an access token issued for Application A for a valid OKD4 API token, the following requirements must be met:
* Application A is granted permission to exchange its tokens for valid tokens accepted by OKD4 cluster authenticator (this is configured on the [Application Portal](https://application-portal.web.cern.ch))
* Application A exchanges user access token following the instructions in the [auth docs]([Token Exchange](https://auth.docs.cern.ch/user-documentation/oidc/exchange-for-api/)
* Exchanged token can then be sent as the bearer token to `/openshift-api-token` endpoint
* Both applications should use Application Registrations configured with the same LoA and the same set of roles - this is a requirement which prevents exchanging tokens between applications with different LoAs (e.g. we should not be able to exchange an SSO token for an OKD API token for a user who is not able to log into the OKD cluster but at the same time can log into the application that performs token exchange)
