FROM registry.cern.ch/docker.io/library/golang:1.22 AS builder
WORKDIR /build
COPY . .
RUN CGO_ENABLED=0 go build -o okd-api-sso-token-exchange

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
COPY --from=builder /build/okd-api-sso-token-exchange /usr/bin/
ENTRYPOINT ["/usr/bin/okd-api-sso-token-exchange"]
