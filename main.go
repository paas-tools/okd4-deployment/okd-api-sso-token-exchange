package main

import (
	"context"
	"crypto/sha256"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"flag"
	"fmt"
	"log"
	"strings"

	kerrs "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"

	configv1 "github.com/openshift/api/config/v1"
	oauthv1 "github.com/openshift/api/oauth/v1"
	"github.com/openshift/library-go/pkg/config/helpers"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"

	oauthclient "github.com/openshift/client-go/oauth/clientset/versioned/typed/oauth/v1"
	userclient "github.com/openshift/client-go/user/clientset/versioned/typed/user/v1"

	"github.com/MicahParks/keyfunc"
	userapi "github.com/openshift/api/user/v1"
)

const (
	sha256Prefix     string = "sha256~"
	defaultExpiresIn int64  = 86400
	// Until August 2022, we have been using the `cern_upn` claim to uniquely identify each user. Unfortunately, this proved to be
	// problematic since in some cases, this claim may be missing from the JWT token (for example when an API Access token
	// is generated - https://auth.docs.cern.ch/user-documentation/oidc/api-access/). To mitigate this issue, `sub` claim
	// is used which is the de facto standard and is guaranteed to be a duplicate of the `cern_upn` claim.
	// https://auth.docs.cern.ch/user-documentation/oidc/config/
	userNameClaim string = "sub"
	// Adjusts the amount of entropy used for generating tokens
	// https://github.com/openshift/oauth-server/blob/04985077512fec241a5170074bf767c23592d7e7/pkg/server/crypto/random.go#L28
	randomBitsForToken int = 256
)

var (
	keyFunc jwt.Keyfunc
	// we set scopes to user:full since it allows full read/write access to the API with all of the user’s permissions
	scopes           []string = []string{"user:full"}
	kubeClientConfig *rest.Config
	oauthClient      *oauthclient.OauthV1Client
	userClient       *userclient.UserV1Client
	audience         *string
	clientName       *string
	jwksURL          *string
	tokenExpiresIn   *int64
)

type OpenshiftApiRequestCtx struct {
	accessToken string
	redirectURI string
	userInfo    UserInfo
}

type UserInfo struct {
	userName string
	fullName string
}

func trimSHA256Prefix(code string) string {
	return strings.TrimPrefix(code, sha256Prefix)
}

func sha256Token(token string) string {
	// We simulate the same technique used by openshift components to create new OAuthAccessTokens.
	// If OKD API server handles a request with a Bearer token in the form of `sha256~<value>`, it will
	// compute the sha256 hash of <value> and query for a OAuthAccessToken whose name is `sha256~<hash>`.
	// If such token exists the server will authenticate the request as the user stored in the token.
	h := sha256.Sum256([]byte(token))
	return sha256Prefix + base64.RawURLEncoding.EncodeToString(h[0:])
}

func tokenToObjectName(token string) string {
	name := trimSHA256Prefix(token)
	return sha256Token(name)
}

func openshiftApiToken(ctx *gin.Context) {
	authorizationHeader := ctx.Request.Header.Get("Authorization")
	if authorizationHeader == "" {
		ctx.JSON(401, gin.H{"error": "authorization header is missing or its value is an empty string"})
		return
	}

	token := strings.Split(authorizationHeader, "Bearer ")
	if len(token) != 2 {
		ctx.JSON(401, gin.H{"error": "value of the Authorization header is invalid"})
		return
	}

	jwtToken, err := parseAndVerifyToken(token[1])
	if err != nil || !jwtToken.Valid {
		log.Print(err.Error())
		ctx.JSON(401, gin.H{"error": err.Error()})
		return
	}

	claims, ok := jwtToken.Claims.(jwt.MapClaims)
	if !ok {
		ctx.JSON(401, gin.H{"error": errors.New("invalid JWT claims")})
		return
	}

	// validate the aud field
	// this ensure that the token has been issued for the correct Application
	aud := claims["aud"].(string)
	if aud != *audience {
		ctx.JSON(401, gin.H{"error": "provided token has been issued for a different application"})
		return
	}

	userName, ok := claims[userNameClaim].(string)
	if !ok {
		ctx.JSON(400, gin.H{"error": fmt.Sprintf("missing %s claim", userNameClaim)})
	}

	fullName, ok := claims["name"].(string)
	if !ok {
		// API tokens don't contain `name` claim thus we need to use the `sub`
		fullName = userName
	}

	userInfo := UserInfo{
		userName: userName,
		fullName: fullName,
	}

	oauthAccessToken := sha256Prefix + randomToken()
	redirectURI := ctx.Request.URL.Query().Get("redirect-uri")
	if redirectURI == "" {
		ctx.JSON(400, gin.H{"error": "empty redirect-uri query parameter"})
		return
	}

	tokenRequestCtx := &OpenshiftApiRequestCtx{accessToken: oauthAccessToken, redirectURI: redirectURI, userInfo: userInfo}
	err = saveOpenshiftAPIToken(tokenRequestCtx)
	if err != nil {
		log.Print(err.Error())
		ctx.JSON(500, gin.H{"error": "something went wrong"})
		return
	}

	ctx.JSON(200, gin.H{"token": oauthAccessToken})
}

func parseAndVerifyToken(token string) (*jwt.Token, error) {
	jwtToken, err := jwt.Parse(token, keyFunc)

	if err != nil {
		return &jwt.Token{}, fmt.Errorf("error while parsing the token: \"%v\"", err)
	}

	if !jwtToken.Valid {
		return &jwt.Token{}, errors.New("token is invalid")
	}

	return jwtToken, nil
}

func saveOpenshiftAPIToken(tokenRequestCtx *OpenshiftApiRequestCtx) error {
	user, err := getOrCreateUser(tokenRequestCtx.userInfo)
	if err != nil {
		return err
	}

	oauthToken := &oauthv1.OAuthAccessToken{
		ClientName: *clientName,
		Scopes:     scopes,
		UserName:   user.Name,
		ObjectMeta: metav1.ObjectMeta{
			Name: tokenToObjectName(tokenRequestCtx.accessToken),
		},
		ExpiresIn:    *tokenExpiresIn,
		RefreshToken: "",
		RedirectURI:  tokenRequestCtx.redirectURI, // we set it to the webservices-portal URL
		UserUID:      string(user.UID),
	}

	_, err = oauthClient.OAuthAccessTokens().Create(context.TODO(), oauthToken, metav1.CreateOptions{})
	if err != nil {
		return err
	}

	return nil
}

func getOrCreateUser(userInfo UserInfo) (*userapi.User, error) {
	user, err := userClient.Users().Get(context.TODO(), userInfo.userName, metav1.GetOptions{})

	if kerrs.IsNotFound(err) {
		newUser := userapi.User{
			ObjectMeta: metav1.ObjectMeta{
				Name: userInfo.userName,
			},
			FullName: userInfo.fullName,
		}

		user, err = userClient.Users().Create(context.TODO(), &newUser, metav1.CreateOptions{})
		if err != nil {
			return nil, err
		}
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

// Copied from https://github.com/openshift/oauth-server/blob/1c79159149541f6f4a1a356a437c3c85c04af41e/pkg/osinserver/tokengen.go
func randomToken() string {
	for {
		// guaranteed to have no / characters and no trailing ='s
		token := randomBitsString(randomBitsForToken)

		// Don't generate tokens with leading dashes... they're hard to use on the command line
		if strings.HasPrefix(token, "-") {
			continue
		}

		return token
	}
}

// Copied from https://github.com/openshift/oauth-server/blob/04985077512fec241a5170074bf767c23592d7e7/pkg/server/crypto/random.go
func randomBitsString(bits int) string {
	size := bits / 8
	if bits%8 != 0 {
		size++
	}
	b := make([]byte, size)
	if _, err := rand.Read(b); err != nil {
		panic(err) // rand should never fail
	}
	return base64.RawURLEncoding.EncodeToString(b)
}

func main() {
	audience = flag.String("audience", "", "only tokens with this audience will be considered valid")
	clientName = flag.String("client-name", "", "the client name used for OAuthAccessToken.ClientName")
	jwksURL = flag.String("jwks-url", "", "jwks endpoint with all information needed to validate the token signature")
	tokenExpiresIn = flag.Int64("token-expires-in", defaultExpiresIn, "number of seconds after which the OAuthAccessTokens expire")

	flag.Parse()

	if *audience == "" {
		panic("-audience argument is required")
	}

	if *clientName == "" {
		panic("-client-name argument is required")
	}

	if *jwksURL == "" {
		panic("-jwks-url argument is required")
	}

	// First create a KeyFunc which returns the public key which is later used to validate the JWT's signature
	jwks, err := keyfunc.Get(*jwksURL, keyfunc.Options{})
	if err != nil {
		panic(err)
	}

	// this is a global variable which is used when validating the JWT token extracted from the Authorization header
	keyFunc = jwks.Keyfunc

	kubeClientConfig, err = helpers.GetKubeConfigOrInClusterConfig("", configv1.ClientConnectionOverrides{})
	if err != nil {
		panic(err)
	}

	oauthClient, err = oauthclient.NewForConfig(kubeClientConfig)
	if err != nil {
		panic(err)
	}

	userClient, err = userclient.NewForConfig(kubeClientConfig)
	if err != nil {
		panic(err)
	}

	r := gin.New()
	r.Use(
		// Do not log request to the ping endpoint
		// https://godocs.io/github.com/gin-gonic/gin#LoggerWithWriter
		gin.LoggerWithWriter(gin.DefaultWriter, "/ping"),
		gin.Recovery(),
	)

	// main handler for token exchange
	r.GET("/openshift-api-token", openshiftApiToken)

	// used only by {readiness,liveness}Probe(s)
	r.GET("/ping", func(ctx *gin.Context) {
		ctx.Data(200, "text/plain", []byte("pong"))
	})

	r.Run(":8080")
}
